<%-- 
    Document   : index.jsp
    Created on : 25/09/2021, 11:14:13
    Author     : Anibal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <head>
        <title>Registro de Vacunación</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- CSS only -->
        <link rel="stylesheet" type="text/css" href="bootstrap.min.css">
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    </head>
</head>
<body>


    <br>        
    <div class="container" >
        <h1>Alumno </h1>
        <form action="NewServlet" method="post">

            <div class="form-group">
                <div class="row">
                    <!-- primera columna -->
                    <div class="col-md-6">
                        <label for="lblCarnet" class="form-label">Carne: </label>
                        <input  class="form-control" type="text" name="InputCarne" placeholder="0910-20-19042">
                    </div>
                    <div class="col-md-6">
                        <label for="lblNombre" class="form-label">Nombre: </label>
                        <input  class="form-control" type="text" name="InputNombres" placeholder="Anibal">
                    </div>

                    <!-- segunda columna -->
                    <div class="col-md-6">
                        <label for="lblApellido" class="form-label">Apellido</label>
                        <input  class="form-control" type="text" name="InputApellidos" placeholder="Morales">
                    </div>

                    <div class="col-md-6">     
                        <label for="lblFacultad" name="InputFacultad" class="form-label">Facultad: </label>
                        <select class="form-control" >
                            <option>Ingenieria</option>
                            <option>Humanidades</option>
                            <option>Cienias Juridicas</option>
                        </select>
                    </div>

                </div>
            </div>
              <input type="submit" class="btn btn-primary btn-lg btn-block" value="Registrar" />
                        
        </form>    
    </div>

</body>
</html>
